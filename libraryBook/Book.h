/*
 * Book.h
 *
 *  Created on: Oct 24, 2013
 *      Author: Alex
 */

#ifndef BOOK_H_
#define BOOK_H_

#include <stdio.h>
#define BOOK_NAME 50
#define AUTHOR_NAME 100
#define LIBRARY 5

struct Book{

	char name[BOOK_NAME];
	char author[AUTHOR_NAME];
	int pages;

};

void displayBook(struct Book bok);
struct Book setName(struct Book bok, char nm[]);
struct Book setAuthor(struct Book bok, char at[]);


#endif /* BOOK_H_ */
