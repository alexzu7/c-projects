/*
 * Book.c
 *
 *  Created on: Oct 24, 2013
 *      Author: Alex
 */

#include "book.h"

void displayBook(struct Book bok){

	printf("Name book -> %s \n",bok.name);
	printf("Name author -> %s \n",bok.author);
	printf("Number of pages -> %d \n\n",bok.pages);
}

struct Book setName(struct Book bok, char nm[]){

	int i;

	for(i = 0; i < BOOK_NAME; i++){
		bok.name[i] = nm[i];
	}
	return bok;
}

struct Book setAuthor(struct Book bok, char at[]){

	int i;

		for(i = 0; i < AUTHOR_NAME; i++){
			bok.author[i] = at[i];
		}
		return bok;
}
