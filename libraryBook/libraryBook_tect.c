/*
 * libraryBook_tect.c
 *
 *  Created on: Oct 24, 2013
 *      Author: Alex
 */
#include "book.h"

int main(){

	struct Book bok1;
	bok1 = setName(bok1,"Master and Margarita");
	bok1 = setAuthor(bok1,"Bulgakov M.");
	bok1.pages = 310;

	struct Book bok2;
	bok2 = setName(bok2,"Crime and Punishment");
	bok2 = setAuthor(bok2,"Dostoevsky F.");
	bok2.pages = 574;

	struct Book bok3;
	bok3 = setName(bok3,"War and Peace");
	bok3 = setAuthor(bok3,"Tolstoy L.");
	bok3.pages = 1274;


	struct Book bok4;
	bok4 = setName(bok4,"Captain's Daughter");
	bok4 = setAuthor(bok4,"Pushkin A.");
	bok4.pages = 320;

	struct Book bok5;
	bok5 = setName(bok5,"Dead Souls");
	bok5 = setAuthor(bok5,"Gogol N.");
	bok5.pages = 352;

	struct Book library[5] = {bok1, bok2, bok3, bok4, bok5};

		int i;

	for(i = 0; i < LIBRARY; i++){
		displayBook(library[i]);
	}


	return 0;
}
